require 'byebug'
# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each.all? {|subs| sub_string?(subs, substring) }
end

def sub_string?(subs, substring)
  subs.split(" ").include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  double_let = []
  string.chars.each.with_index do |l, i|
    if double_let.include?(l)
      next
    elsif l == " "
      next
    else
      string.chars[i+1..-1].each do |l2|
        if l == l2
         double_let << l
         break
       end
     end
    end
  end
  double_let.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  (string.split(" ").sort_by {|word| word.length})[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ('a'..'z').to_a
  alphabet.reject {|let| string.chars.include?(let)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.chars == year.to_s.chars.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  one_week = []
  songs.map.with_index do |s, i|
    if s == songs[i+1] || s == songs[i-1]
      one_week << s
    end
  end
  no_repeats?(songs, one_week).uniq
end

def no_repeats?(songs, one_week)
  songs.reject {|song| one_week.include?(song)}
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  str = ""
  str_array = remove_punctuation(string)
  str_array.each do |word|
    if word.include?("c") == false
      next
    elsif str == ""
      str += word
    elsif c_distance(word) < c_distance(str)
      str = word
    end
  end
  str
end

def c_distance(word)
  word.reverse.index("c")
end

def remove_punctuation(string)
  string.split(" ").map {|word| word.gsub(/\W/, "")}
end



# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4, 3, 3]) => [[2, 3], [4, 6]]


def repeated_number_ranges(arr)
  return_arr = []
  arr.each.with_index do |num1, i1|
    if num1 == arr[i1+1] && num1 != arr[i1-1]
      arr[i1+1..-1].each.with_index do |num2, i2|
        if i1+i2+2 == arr.length
          return_arr << [i1] + [i1+i2+1]
        elsif num1 != num2
          return_arr << [i1] + [i1+i2]
          break
        else
          next
        end
      end
    end
  end
  return_arr
end
